
## installation des certificats letsencrypt https
après install cert-manager with regular manifest:

  

https://cert-manager.readthedocs.io/en/latest/getting-started/install/kubernetes.html#installing-with-regular-manifests

  
  

### ****************** test **************************

#### créer un fichier Clusterissuer staging pour test:

  
```sh
cat <<EOF > letsencrypt-staging-cluster-issuer.yaml
apiVersion: certmanager.k8s.io/v1alpha1
kind: ClusterIssuer
metadata:
  name: letsencrypt-staging-cluster-issuer
spec:
  acme:
    server: https://acme-staging-v02.api.letsencrypt.org/directory
    email: jeanfrancois.rebaud@yahoo.fr
    privateKeySecretRef:
      name: letsencrypt-staging
    http01: {}
EOF


kubectl create -f letsencrypt-staging-cluster-issuer.yaml
```
 
#### verify:

```sh
kubectl describe clusterissuer.certmanager.k8s.io/letsencrypt-staging-cluster-issuer
```
#### update the ingress ressource:
get ingress in the namespace:

```sh
kubectl get ing -n live-assault
get ingress yaml in the ing you must to update
kubectl get ing -n live-assault production-auto-deploy -o yaml
```
#### open your dashboard and add in annotation the certmanager config in the ingress you must to modify like that:

if exemple you have:
```json
"annotations": {

"field.cattle.io/publicEndpoints": "[{\"addresses\":[\"163.172.38.149\"],\"port\":443,\"protocol\":\"HTTPS\",\"serviceName\":\"live-assault:production-auto-deploy\",\"ingressName\":\"live-assault:production-auto-deploy\",\"hostname\":\"jeffreb-live-assault.mytoobox.net\",\"path\":\"/\",\"allNodes\":false},{\"addresses\":[\"163.172.38.149\"],\"port\":443,\"protocol\":\"HTTPS\",\"serviceName\":\"live-assault:production-auto-deploy\",\"ingressName\":\"live-assault:production-auto-deploy\",\"hostname\":\"le.mytoobox.net\",\"path\":\"/\",\"allNodes\":false},{\"addresses\":[\"163.172.38.149\"],\"port\":443,\"protocol\":\"HTTPS\",\"serviceName\":\"live-assault:production-auto-deploy\",\"ingressName\":\"live-assault:production-auto-deploy\",\"hostname\":\"live-assault.com\",\"path\":\"/\",\"allNodes\":false},{\"addresses\":[\"163.172.38.149\"],\"port\":443,\"protocol\":\"HTTPS\",\"serviceName\":\"live-assault:production-auto-deploy\",\"ingressName\":\"live-assault:production-auto-deploy\",\"hostname\":\"www.live-assault.com\",\"path\":\"/\",\"allNodes\":false}]",
"kubernetes.io/ingress.class": "nginx",
"kubernetes.io/tls-acme": "true",
"certmanager.k8s.io/cluster-issuer": "letsencrypt-staging-cluster-issuer" <=== add this line

}
```

click on update

#### verify:

```sh
kubectl describe ing --namespace=live-assault production-auto-deploy

Events:
Type Reason Age From Message
---- ------ ---- ---- -------
Normal UPDATE 88s (x6 over 17h) nginx-ingress-controller Ingress live-assault/production-auto-deploy
Normal CreateCertificate 88s cert-manager Successfully created Certificate "production-auto-deploy-tls"
```
#### verify:
```sh
kubectl describe clusterissuer.certmanager.k8s.io/letsencrypt-staging-cluster-issuer
kubectl get certificates -n live-assault

NAME READY SECRET AGE
production-auto-deploy-tls False production-auto-deploy-tls 18m
```
#### verify:
```sh
kubectl describe certificate production-auto-deploy-tls -n live-assault

 NAME                         READY   SECRET                       AGE
production-auto-deploy-tls   False   production-auto-deploy-tls   18m

verify:
kubectl describe certificate production-auto-deploy-tls -n live-assault

Name:         production-auto-deploy-tls
Namespace:    live-assault
Labels:       app=production
              chart=auto-deploy-app-0.2.9
              heritage=Tiller
              release=production
Annotations:  <none>
API Version:  certmanager.k8s.io/v1alpha1
Kind:         Certificate
Metadata:
  Creation Timestamp:  2019-08-19T14:12:56Z
  Generation:          3
  Owner References:
    API Version:           extensions/v1beta1
    Block Owner Deletion:  true
    Controller:            true
    Kind:                  Ingress
    Name:                  production-auto-deploy
    UID:                   a9d4998a-faaf-4a36-83c7-c1f7f2913679
  Resource Version:        9922231
  Self Link:               /apis/certmanager.k8s.io/v1alpha1/namespaces/live-assault/certificates/production-auto-deploy-tls
  UID:                     0cd70c89-4bc9-4b6f-b586-3fc71bb9bb18
Spec:
  Acme:
    Config:
      Domains:
        le.mytoobox.net
        jeffreb-live-assault.mytoobox.net
        live-assault.com
        www.live-assault.com
      http01:
        Ingress Class:  nginx
  Dns Names:
    le.mytoobox.net
    jeffreb-live-assault.mytoobox.net
    live-assault.com
    www.live-assault.com
  Issuer Ref:
    Kind:       ClusterIssuer
    Name:       letsencrypt-staging-cluster-issuer
  Secret Name:  production-auto-deploy-tls
Status:
  Conditions:
    Last Transition Time:  2019-08-19T14:12:56Z
    Message:               Certificate issuance in progress. Temporary certificate issued.
    Reason:                TemporaryCertificate
    Status:                False
    Type:                  Ready
Events:
  Type    Reason              Age   From          Message
  ----    ------              ----  ----          -------
  Normal  Generated           27m   cert-manager  Generated new private key
  Normal  GenerateSelfSigned  27m   cert-manager  Generated temporary self signed certificate
  Normal  OrderCreated        27m   cert-manager  Created Order resource "production-auto-deploy-tls-302661120"
```

### *********************** production ************************************

#### créer un fichier Clusterissuer prod:
```sh
cat <<EOF > letsencrypt-staging-cluster-issuer.yaml
apiVersion: certmanager.k8s.io/v1alpha1
kind: ClusterIssuer
metadata:
  name: letsencrypt-prod-cluster-issuer
spec:
  acme:
    server: https://acme-v02.api.letsencrypt.org/directory
    email: jeanfrancois.rebaud@yahoo.fr
    privateKeySecretRef:
      name: letsencrypt-prod
    http01: {}
EOF

kubectl create -f letsencrypt-prod-cluster-issuer.yaml
```
#### verify:

```sh
kubectl describe clusterissuer.certmanager.k8s.io/letsencrypt-prod-cluster-issuer
```
#### open your dashboard and add in annotation the certmanager config in the ingress you must to modify like that:

if exemple you have

```json
"annotations": {
"field.cattle.io/publicEndpoints": "[{\"addresses\":[\"163.172.38.149\"],\"port\":443,\"protocol\":\"HTTPS\",\"serviceName\":\"live-assault:production-auto-deploy\",\"ingressName\":\"live-assault:production-auto-deploy\",\"hostname\":\"jeffreb-live-assault.mytoobox.net\",\"path\":\"/\",\"allNodes\":false},{\"addresses\":[\"163.172.38.149\"],\"port\":443,\"protocol\":\"HTTPS\",\"serviceName\":\"live-assault:production-auto-deploy\",\"ingressName\":\"live-assault:production-auto-deploy\",\"hostname\":\"le.mytoobox.net\",\"path\":\"/\",\"allNodes\":false},{\"addresses\":[\"163.172.38.149\"],\"port\":443,\"protocol\":\"HTTPS\",\"serviceName\":\"live-assault:production-auto-deploy\",\"ingressName\":\"live-assault:production-auto-deploy\",\"hostname\":\"live-assault.com\",\"path\":\"/\",\"allNodes\":false},{\"addresses\":[\"163.172.38.149\"],\"port\":443,\"protocol\":\"HTTPS\",\"serviceName\":\"live-assault:production-auto-deploy\",\"ingressName\":\"live-assault:production-auto-deploy\",\"hostname\":\"www.live-assault.com\",\"path\":\"/\",\"allNodes\":false}]",
"kubernetes.io/ingress.class": "nginx",
"kubernetes.io/tls-acme": "true",
"certmanager.k8s.io/cluster-issuer": "letsencrypt-prod-cluster-issuer" <=== add this line

}
```

click on update
#### verify:
```sh
kubectl describe ing --namespace=live-assault production-auto-deploy

Events:
  Type     Reason             Age                From                      Message
  ----     ------             ----               ----                      -------
  Warning  BadConfig          60m                cert-manager              ClusterIssuer resource "cert-manager/letsencrypt-staging" not found
  Warning  BadConfig          51m                cert-manager              ClusterIssuer resource "letsencrypt-staging/cert-manager" not found
  Normal   CreateCertificate  46m                cert-manager              Successfully created Certificate "production-auto-deploy-tls"
  Normal   UPDATE             37s (x7 over 18h)  nginx-ingress-controller  Ingress live-assault/production-auto-deploy
  Normal   UpdateCertificate  37s                cert-manager              Successfully updated Certificate "production-auto-deploy-tls"
```
#### verify:
```sh
kubectl describe clusterissuer.certmanager.k8s.io/letsencrypt-prod-cluster-issuer
kubectl get certificates -n live-assault

 NAME                         READY   SECRET                       AGE
production-auto-deploy-tls   True    production-auto-deploy-tls   48m
```

#### verify:

```sh
kubectl describe certificate production-auto-deploy-tls -n live-assault

Name:         production-auto-deploy-tls
Namespace:    live-assault
Labels:       app=production
              chart=auto-deploy-app-0.2.9
              heritage=Tiller
              release=production
Annotations:  <none>
API Version:  certmanager.k8s.io/v1alpha1
Kind:         Certificate
Metadata:
  Creation Timestamp:  2019-08-19T14:12:56Z
  Generation:          5
  Owner References:
    API Version:           extensions/v1beta1
    Block Owner Deletion:  true
    Controller:            true
    Kind:                  Ingress
    Name:                  production-auto-deploy
    UID:                   a9d4998a-faaf-4a36-83c7-c1f7f2913679
  Resource Version:        9930144
  Self Link:               /apis/certmanager.k8s.io/v1alpha1/namespaces/live-assault/certificates/production-auto-deploy-tls
  UID:                     0cd70c89-4bc9-4b6f-b586-3fc71bb9bb18
Spec:
  Acme:
    Config:
      Domains:
        le.mytoobox.net
        jeffreb-live-assault.mytoobox.net
        live-assault.com
        www.live-assault.com
      http01:
        Ingress Class:  nginx
  Dns Names:
    le.mytoobox.net
    jeffreb-live-assault.mytoobox.net
    live-assault.com
    www.live-assault.com
  Issuer Ref:
    Kind:       ClusterIssuer
    Name:       letsencrypt-prod-cluster-issuer
  Secret Name:  production-auto-deploy-tls
Status:
  Conditions:
    Last Transition Time:  2019-08-19T14:59:42Z
    Message:               Certificate is up to date and has not expired
    Reason:                Ready
    Status:                True
    Type:                  Ready
  Not After:               2019-11-17T13:59:40Z
Events:
  Type    Reason              Age    From          Message
  ----    ------              ----   ----          -------
  Normal  Generated           49m    cert-manager  Generated new private key
  Normal  GenerateSelfSigned  49m    cert-manager  Generated temporary self signed certificate
  Normal  OrderCreated        49m    cert-manager  Created Order resource "production-auto-deploy-tls-302661120"
  Normal  Cleanup             3m19s  cert-manager  Deleting old Order resource "production-auto-deploy-tls-302661120"
  Normal  OrderCreated        3m19s  cert-manager  Created Order resource "production-auto-deploy-tls-2337039838"
  Normal  OrderComplete       2m49s  cert-manager  Order "production-auto-deploy-tls-2337039838" completed successfully
  Normal  CertIssued          2m49s  cert-manager  Certificate issued successfully
```




READY !!!!!!!!!!!!
