# CI/CD avec Kubernetes - GitLab - AutoDevOps

[![N|Solid](https://cldup.com/dTxpPi9lDf.thumb.png)](https://nodesource.com/products/nsolid)

[![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)](https://travis-ci.org/joemccann/dillinger)

Prodédure d'installation d'un serveur kunernetes et de ses connexions avec GitLab pour multiples projets d'intégration
continue.

# Initialiser le serveur

- Sécuriser SSH
    ```sh
    $: ssh user@ip
    user@server:~$: su -
    root@server:~$: nano /etc/ssh/sshd_config
    --------------------
    Port 2223
    PermitRootLogin no
    --------------------
    root@ip:~$: service ssh restart
    ```
- installer la clef public RSA
    ```sh
    root@server:~$: exit
    user@server:~$: exit
    $: ssh user@ip -p 2223
    user@server:~$: nano .ssh/authorized_keys
    --------------------
    ssh-rsa AAAAB.......
    --------------------
    user@server:~$: exit
    $: ssh user@servername # régler votre .ssh/config en cconséquence (name host, private key)
    user@server:~$: su -
    root@server:~$: nano /etc/ssh/sshd_config
    --------------------
    PasswordAuthentication no
    PermitEmptyPasswords no
    AllowUsers jeff
    --------------------
    root@ip:~$: service ssh restart
    ```
- installer sudo et parametrer user en sudouser
    ```sh
    user@server:~$: su -
    root@server:~$: apt-get install sudo
    root@server:~$: adduser user sudo
    ```
- vérifier sudo user et les logs access
    ```sh
    root@server:~$: exit
    user@server:~$: exit
    $: ssh user@kubernetes # connexion au host après configuration du ficher local poste client .ssh/config
                       # Host kubernetes
                            # HostName xxx.xxx.xx.xx (ip server)
                        	# user xxx
                            # Port 2223
                            # IdentityFile C:\Users\xxx\.ssh\id_jeff_git_online2
    user@server:~$: sudo apt-get update
    user@server:~$: sudo grep "Invalid" /var/log/auth.log

    ```

# Sécuriser le serveur
> Si vous installer Rancher, Prometheus, Gtafana et Elastic Stakc, n'installer pas le pare-feu maintenant
> mais plutôt après avoie installer ces application en prenant soins de noter leurs ports d'accès
> et permettre lors de l'installation de UFW de les laisser ouvert

## UFW Gestion du pare feu
### Installer la gestion du parfeu

```sh
user@server:~$: sudo apt-get install ufw

```

### Définir les politiques de sécurité

```sh
user@server:~$: sudo ufw default deny  # par défaut on refuse tout
user@server:~$: sudo ufw default allow outgoing # ensuite on autorise le trafic entrant

# Ensuite on établie les règles

user@server:~$: sudo ufw allow 2223/tcp # Port SSH configurer plus haut !!! INDISPENSABLE
user@server:~$: sudo ufw allow 80/tcp # Serveur http
user@server:~$: sudo ufw allow 81/tcp # Serveur http rancher
user@server:~$: sudo ufw allow 443/tcp # Serveur https
user@server:~$: sudo ufw allow 444/tcp # Serveur https rancher
user@server:~$: sudo ufw allow 32322/tcp # Serveur http prometheus
user@server:~$: sudo ufw allow 32323/tcp # Serveur http grafana
user@server:~$: sudo ufw allow 6443/tcp # Port Kubernetes API

```

### Activé les nouvelles règles

```sh
user@server:~$: sudo ufw enable

```

### Visualisé les résultats

```sh
user@server:~$: sudo ufw status numbered

```

### Ajouter de nouvelles règles

```sh
user@server:~$: sudo ufw allow 25/TCP

```

### Supprimer des règles

```sh
user@server:~$: sudo ufw delete NUMERO

```

> Pour aller plus loin avec ufv voir:

Voir: [ufw tuto online](https://documentation.online.net/fr/dedicated-server/tutorials/security/configure-firewall)
Voir: [wiki ubuntu ufw](https://wiki.ubuntu.com/UncomplicatedFirewall)


## Fail2Ban

protection supplémentaire
Permet de lutter contre les attaques brut force
il suffit juste de l'installer comme ceci:

```sh
user@server:~$: sudo apt install fail2ban

```

> Pour aller plus loin avec Fail2Ban voir:


Voir: [installer et parametrer Fail2Ban](https://buzut.net/installer-et-parametrer-fail2ban/)

REDEMARRER LE SERVEUR POUR QUE LE PARE-FEU SOI PRIS EN COMPTE

# Récupérer les fichiers yaml du dépot
### Installer git
```sh
        user@server:~$: sudo apt-get update
        user@server:~$: sudo apt-get install git

```
### Générer une paire de clef SSH-rsa
```sh
        user@server:~$: cd .ssh/
        user@server:~/.ssh/$: ssh-keygen -o -t rsa -b 4096 -C "email@example.com"
        user@server:~/.ssh/$: cd

```
### Ajouter votre clef public à votre compte gitlab
```sh
        user@server:~$: sudo apt-get install xclip
        user@server:~$: xclip -sel clip < ~/.ssh/id_ed25519.pub

        ou

        user@server:~$: cat ~/.ssh/id_ed25519.pub
        puis copier coller

```
coller votre clef public dans gitlab->compte->parametres->clefs SSH

### Configurer votre serveur pour utiliser les clefs
```sh
        user@server:~$: nano ~/.ssh/config
        -------------------------------------------
        ...
        Host gitlab
          HostName gitlab.com
          Preferredauthentications publickey
          IdentityFile ~/.ssh/gitlab2
        ...
        -------------------------------------------

```
### Tester votre connexion
```sh
        user@server:~$: ssh -T git@gitlab

```
### Cloner le dépot (vérifier avant l'emplacement actuel ~$: racine user)
```sh
        user@server:~$: git clone git@gitlab:JeffReb/prepa-environnement.git
        user@server:~$: cd prepa-environnement/
        user@server:~$: cp -r *.* ../
        user@server:~$: cp -r volumes/ ../
        user@server:~$: cd
        user@server:~$: rm -rf prepa-environnement/


```


# Installer docker-ce (18.06 - validé)
Voir: [Docker CE Installation](https://docs.docker.com/install/linux/docker-ce/debian/)
....
installer uniquement docker-ce:
```sh
        user@server:~$: sudo apt-get install docker-ce

```
### Préparer docker pour Kubernetes (activer docker au démarrage )
```sh
        user@server:~$: su -
        root@server: nano /etc/docker/daemon.json
        -------------------------------------------
        {
          "exec-opts": ["native.cgroupdriver=systemd"],
          "log-driver": "json-file",
          "log-opts": {
            "max-size": "100m"
          },
          "storage-driver": "overlay2"
        }
        -------------------------------------------
        root@server: mkdir -p /etc/systemd/system/docker.service.d
        root@server: systemctl daemon-reload
        root@server: systemctl restart docker        

```

Voir: [Manage Docker as non-root user](https://docs.docker.com/install/linux/linux-postinstall/)

vérifier que l'utilisateur user puisse lancer docker sans sudo:
1. usermod -aG docker $USER en root

```sh
        user@server:~$: sudo usermod -aG docker $USER   

```
2. se déconnecter puis se reconnecter

```sh
        user@server:~$: exit
        ~$: ssh user@server
        user@server:~$: docker ps

```

### Installer docker-compose

Voir: [Docker Compose](https://docs.docker.com/compose/install/)

```sh
user@server:~$: sudo curl -L "https://github.com/docker/compose/releases/download/1.24.0/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
user@server:~$: sudo chmod +x /usr/local/bin/docker-compose
user@server:~$: docker-compose --version

```


# Installer kubernetes
### désactiver le swap
```sh
        user@server:~$: su -
        root@server:~$: swapoff -a
        root@server:~$: nano /etc/fstab
        -------------------------------------------
        commenter la ligne swap: # ....
        -------------------------------------------
        root@server:~$: free
```
### installer kubernetes
```sh
        root@server:~$: curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key add -
        root@server:~$: add-apt-repository "deb http://apt.kubernetes.io/ kubernetes-xenial main"
        root@server:~$: apt-get update
        root@server:~$: apt-get install -y kubelet kubeadm kubectl
```

> Répéter toutes ses opérations depuis de début (initialiser le serveur) sur les serveur workers (worker nodes).
> Les opérations suivantes ne sont à réaliser que sur le master hormis pour la jonction des
> workers nodes sur le master (voir plus loin...)


This text you see here is *actually* written in Markdown! To get a feel for Markdown's syntax, type some text into the left window and watch the results in the right.

### Initialiser le master kubernetes
```sh
        root@master:~$: kubeadm init --pod-network-cidr=10.244.0.0/16

        ######### copier dans un ficher texte le résultat suivant:  ###############
        #### surtout la ligne kubeadm join xxx.xxx.xx.xx:6443.......sha256:b261cb..
        ##########################################################################

        **************************************************************
		Your Kubernetes master has initialized successfully!
        To start using your cluster, you need to run the following as a regular user:

          mkdir -p $HOME/.kube
          sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
          sudo chown $(id -u):$(id -g) $HOME/.kube/config

        You should now deploy a pod network to the cluster.
        Run "kubectl apply -f [podnetwork].yaml" with one of the options listed at:
          https://kubernetes.io/docs/concepts/cluster-administration/addons/

        You can now join any number of machines by running the following on each node
        as root:

          kubeadm join xxx.xxx.xx.xx:6443 --token l1huoi.gdt1cem4rjr4049p --discovery-token-ca-cert-hash sha256:b261cbf1005bcae292b9e71c6473032950503617c4811b47547ef3ef2beab54e

        ****************************************************************************************************
```
### Configurer l'accès utilisateur au master kubernetes
```sh
        root@master:~$: exit
        user@master:~$: mkdir -p $HOME/.kube
        user@master:~$: sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
        user@master:~$: sudo chown $(id -u):$(id -g) $HOME/.kube/config
```
# Joindre les workers nodes au master

Si vous avez plusieurs servers il faut les reliés au master en tant que worker reprendre les étapes d'installations vuent précédements sur chaque worker et ensuite lancez la commande de demande de ratachement suivante:
```sh
        user@worker01:~$: kubeadm join xxx.xxx.xx.xx:6443 --token l1huoi.gdt1cem4rjr4049p --discovery-token-ca-cert-hash sha256:b261......

```
Si vous n'avez qu'un seul server master il faut le configurer pour qu'il puisse executer les pods (containers) sur sa propre machine (ce qui n'est pas activer par défaut):
```sh
        user@master:~$: kubectl taint nodes --all node-role.kubernetes.io/master-

```
# Configurer le réseau interne de kubernetes
Pour cela nous allons installer CANAL
```sh
        user@master:~$: kubectl apply -f https://docs.projectcalico.org/v2.6/getting-started/kubernetes/installation/hosted/canal/rbac.yaml
        user@master:~$: kubectl apply -f https://docs.projectcalico.org/v2.6/getting-started/kubernetes/installation/hosted/canal/canal.yaml
```
Vérifions maintenant si toute notre installation fonctionne bien la commande suivante fait apparaître tout les pods lancer et affiche leurs états de fonctionnement:
```sh
        user@master:~$: kubectl get pods --all-namespaces -o wide
```
# Installer un reverse proxy
Puisque nous somme sur un cluster 'Barre Metal' et non un cloud comme Google Kubernetes ou Amazon nous devons installer un reverse proxy il permet d'affecter notre adresse IP externe (public) a Kubernetes afin que le monde exterieur puisse pointer sur nos applications en l'affectant a un 'load balancer' que nous installeront plus tard

### installation du reverse proxy metallB
```sh
        user@master:~$: kubectl apply -f https://raw.githubusercontent.com/google/metallb/v0.7.3/manifests/metallb.yaml
```
### configuration de metallB
```sh
        user@master:~$: nano config-metalb.yaml
        -----------------------------------------------
        apiVersion: v1
        kind: ConfigMap
        metadata:
          namespace: metallb-system
          name: config
        data:
          config: |
            address-pools:
            - name: default
              protocol: layer2
              addresses:
              - xxx.xxx.xx.xx-yyy.yyy.yy.yy # plage d'adresse IP de toutes nos machines elles doivent
                                            # faire partie d'un même réseau interne comme dans toute config
                                            # barre metal (exemple réseau privé RPN de chez
                                            # Online - https://www.online.net/fr/serveur-dedie/rpnv2
                                            # (si une seule machine yyy... = xxx...)
        ---------------------------------------------------------------------------------------------------
        user@master:~$: kubectl apply -f config-metalb.yaml
```
# Installer le Dashboard de kubernetes
### Installer le Dashboard
```sh
        user@master:~$: kubectl apply -f https://raw.githubusercontent.com/kubernetes/dashboard/master/aio/deploy/recommended/kubernetes-dashboard.yaml
```
### Creer un fichier utilisateur admin-user.yaml
```sh
        user@master:~$: nano admin-user.yaml
        --------------------------------------------------------
        apiVersion: v1
        kind: ServiceAccount
        metadata:
          name: admin-user
          namespace: kube-system
        --------------------------------------------------------
```
### Creer le rôle qui lui sera attaché
```sh
        user@master:~$: nano admin-role.yaml
        --------------------------------------------------------
        apiVersion: rbac.authorization.k8s.io/v1beta1
        kind: ClusterRoleBinding
        metadata:
          name: admin-user
        roleRef:
          apiGroup: rbac.authorization.k8s.io
          kind: ClusterRole
          name: cluster-admin
        subjects:
        - kind: ServiceAccount
          name: admin-user
          namespace: kube-system
        --------------------------------------------------------
```
### Chargez ces configurations dans le cluster
```sh
        user@master:~$: kubectl apply -f admin-user.yaml
        user@master:~$: kubectl apply -f admin-role.yaml
```
### Récupérer le token de connexion
```sh
        user@master:~$: kubectl -n kube-system describe secret $(kubectl -n kube-system get secret | grep admin-user | awk '{print $1}')
        -----------------------------
        Name:         admin-user-token-6tnqd
        Namespace:    kube-system
        Labels:       <none>
        Annotations:  kubernetes.io/service-account.name: admin-user
                      kubernetes.io/service-account.uid: ce24c190-43e1-11e9-9cd4-0008a20b43fc

        Type:  kubernetes.io/service-account-token

        Data
        ====
        token:      eyJhb.................................................
        ca.crt:     1025 bytes
        namespace:  11 bytes
        -------------------------------------------------------------------------------------
```
# Acceder au Dashboard
### ouvrir deux nouvelles fenêtre de terminal
* Sur la première fenêtre se reconnecter en ssh au master et lancer le proxy k8s
```sh
        user@master:~$: kubectl proxy
```

* Sur la deuxième lancer une connexion via un tunel SSH
```sh
        $: ssh -L 8001:127.0.0.1:8001 user@xxx.xxx.xx.xx -p 2223 # ip du serveur Master
```
* Accèder au Dashboard via vôtre navigateur a l'adresse:
 [dashboard](http://127.0.0.1:8001/api/v1/namespaces/kube-system/services/https:kubernetes-dashboard:/proxy/#!/login)
http://127.0.0.1:8001/api/v1/namespaces/kube-system/services/https:kubernetes-dashboard:/proxy/#!/login
* rentrez le token de connexion récupérer plus haut
```sh
        Data
        ====
        token:      eyJhb.......
```

# Montage des volumes dynamiques pour la persistence des données de vos applications
Dans cette partie nous considérons que les volumes sont situés à un emplacement (dossiers) physique du disque
principal du serveur master (/srv/nfs/kubedata).

Dans le cadre d'un cluster équipé pour la production la méthode est complètement différente le système doit être équiper d'un autre orchestrateur spécialisé pour la gestion des volumes tel que Ceph par exmple (équivalent de kubernetes pour les disques dur et les data) la gestion des volumes est dans ce cas précis pris en charge par l'orchestrateur que l'on doit paramètrer afin qu'il se connecte aux différents disques externes (autres que le disque principal du master) mis a disposition dans le cluster.
Ce système prend en charge la duplication et la répartition des données en les clonnant et en formatant les disques dans
un format spécifique
Losqu'il est paramétrer et fonctionnel, il  sert ensuite de provider à kubernetes pour lui mettre à disposition les volumes dont il a la charge d'orchèstrer.
Il ne reste plus qu'a ensuite paramètrer Kubernetes pour lui indiqué que la prise en charge des volume est réalisé par un provider orchestrateur tel que Ceph par exemple.

Nous allons nous intérreser à la mise en place des volumes sans ce type de méthode avec seulement à disposition notre serveur master comme hebergeur des données persistante dans un dossier réserver a cet effet que nous créront sur son disque principal
### Montage des volumes

* préparer l'emplacement (créer des dossiers suivant le nombre de vos applis et de leur data dont vous avez besoin)
```sh
        user@master:~$: sudo mkdir /srv/nfs/kubedata -p
        user@master:~$: sudo chown nobody: /srv/nfs/kubedata

```
### Installer un server NFS

```sh
        user@master:~$: sudo apt-get install nfs-kernel-server nfs-common
        user@master:~$: sudo apt-get install cifs-utils
        user@master:~$: sudo systemctl enable nfs-server
        user@master:~$: sudo systemctl start nfs-server


```

* tester le serveur NFS
```sh
        user@master:~$: sudo nano /etc/exports
        --------------------------------------
        ## add the following line in the end of file
        /srv/nfs/kubedata  *(rw,sync,no_subtree_check,no_root_squash,no_all_squash,insecure)
        --------------------------------------
        user@master:~$: sudo exportfs -rav
        user@master:~$: sudo exportfs -v # vérifier
        user@master:~$: sudo mount -t nfs 127.0.0.1:/srv/nfs/kubedata /mnt
        user@master:~$: df
        user@master:~$: mount | grep kubedata
        user@master:~$: sudo umount /mnt

```
### Installer le clien NFS dans kubernetes


```sh
        user@master:~$: cd volumes/nfs-client
        user@master:~volumes/nfs-client$: kubectl create -f rbac.yaml
        user@master:~volumes/nfs-client$: kubectl create -f class.yaml
        user@master:~volumes/nfs-client$: kubectl create -f deployment.yaml

```
* définir la class managed-nfs-storage comme class par défault pour recevoir les affectations des demandes des applications
```sh
        user@master:~volumes/nfs-client$: kubectl patch storageclass managed-nfs-storage -p '{"metadata": {"annotations":{"storageclass.kubernetes.io/is-default-class":"true"}}}'
        user@master:~volumes/nfs-client$: kubectl get storageclass
        Resultat:
					*************************************************************************
					NAME                      PROVISIONER                    AGE
					managed-nfs-storage (default)   kubernetes.io/no-provisioner   68m
					*************************************************************************


```

* tester l'ensemble client nfs - storageclass
```sh
        user@master:~volumes/nfs-client$: kubectl create -f pvc-test.yaml
        user@master:~volumes/nfs-client$: kubectl create -f pvc-test-auto.yaml
        user@master:~volumes/nfs-client$: kubectl get pv,pvc
        user@master:~volumes/nfs-client$: ls /srv/nfs/kubedata
        user@master:~volumes/nfs-client$: kubectl delete -f pvc-test.yaml
        user@master:~volumes/nfs-client$: kubectl delete -f pvc-test-auto.yaml
        user@master:~volumes/nfs-client$: ls /srv/nfs/kubedata
        user@master:~volumes/nfs-client$: kubectl get pv,pvc


```
# SPECIAL NOTE FOR MYSQL: NFS IS NOT GUARENTED TO USE WITH MYSQL WE TAKE OTHER LOCAL STORAGE SOLUTION FOR THIS
## Volumes mount on kubernetes with static local-storage
### Montage des volumes

* récupérer les noms des différents noeuds
```sh
        user@master:~$: kubectl get nodes
        -------------------------------------
        ***********************************************
    	NAME        STATUS   ROLES    AGE   VERSION
    	sd-118706   Ready    master   22m   v1.13.4
    	***********************************************
    	-------------------------------------
```
* préparer l'emplacement (créer des dossiers suivant le nombre de vos applis et de leur data dont vous avez besoin)
```sh
        user@master:~$: cd prepa-environnement/
        user@master:~$: cp -r volumes/ ../
        user@master:~$: cd
        user@master:~$: cd volumes
        user@master:~$: mkdir app1
        user@master:~$: mkdir app2
        user@master:~$: mkdir app...

```
ou lancer le script prévu a cet effet
```sh
        user@master:~$: cd volumes
        user@master:~$: chmod +x mkdir.sh
        user@master:~$: ./mkdir.sh
        user@master:~$: cd
        user@master:~$: chmod -R 777 volumes/ # donner les droits d'accès maxi

```

* creer une class local storage
```sh
        user@master:~$: nano storage-class.yml
        --------------------------------------
        kind: StorageClass
        apiVersion: storage.k8s.io/v1
        metadata:
          name: local-storage
        provisioner: kubernetes.io/no-provisioner
        volumeBindingMode: WaitForFirstConsumer
        --------------------------------------
        user@master:~$: kubectl create -f storage-class.yml

```
* creer les volumes peristent dans kubernetes
```sh
        user@master:~$: nano nano pv
        --------------------------------------
        apiVersion: v1
        kind: PersistentVolume
        metadata:
          name: local-pv1
        spec:
          capacity:
            storage: 20Gi
          accessModes:
          - ReadWriteOnce
          persistentVolumeReclaimPolicy: Retain
          storageClassName: local-storage
          local:
            path: /home/user/volumes/app1
          nodeAffinity:
            required:
              nodeSelectorTerms:
              - matchExpressions:
                - key: kubernetes.io/hostname
                  operator: In
                  values:
                  - sd-118706 # <= nom de votre noeud (kubectl get nodes)
        ---
        apiVersion: v1
        kind: PersistentVolume
        metadata:
          name: local-pv2
        spec:
          capacity:
            storage: 20Gi
          accessModes:
          - ReadWriteOnce
          persistentVolumeReclaimPolicy: Retain
          storageClassName: local-storage
          local:
            path: /home/user/volumes/app2
          nodeAffinity:
            required:
              nodeSelectorTerms:
              - matchExpressions:
                - key: kubernetes.io/hostname
                  operator: In
                  values:
                  - sd-118706
        ---
        apiVersion: v1
        kind: PersistentVolume
        metadata:
          name: local-pv3
        spec:
          capacity:
            storage: 20Gi
          accessModes:
          - ReadWriteOnce
          persistentVolumeReclaimPolicy: Retain
          storageClassName: local-storage
          local:
            path: /home/user/volumes/app3
          nodeAffinity:
            required:
              nodeSelectorTerms:
              - matchExpressions:
                - key: kubernetes.io/hostname
                  operator: In
                  values:
                  - sd-118706
        ---
        apiVersion: v1
        kind: PersistentVolume
        metadata:
          name: local-pv...
        ...
            local:
            path: /home/user/volumes/app......
            ...
        --------------------------------------
        user@master:~$: kubectl create -f pv.yml

```
* définir la class local storage comme class a utiliser pour installation manuelle Mysql via Rancher


1. Dans Rancher selectionnner la class local-storage




# Installation manuelle du load balancer 'Ingress' sur kubernetes
### Installation sur votre serveur master
```sh
        user@master:~$: kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/nginx-0.24.1/deploy/mandatory.yaml
        user@master:~$: kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/nginx-0.24.1/deploy/provider/cloud-generic.yaml
```
### vérification de l'ip affecter à ingress via metallB
```sh
        user@master:~$: kubectl get pods --all-namespaces -o wide
        user@master:~$: kubectl get svc --all-namespaces
        Résultat:
        ************************************************************************************
        NAME            TYPE        CLUSTER-IP    EXTERNAL-IP     PORT(S)   ......
        ingress-nginx   LoadBalancer 10.101.9.4   xxx.xxx.xx.xx   80:32532/TCP,443:31736/TCP
        *************************************************************************************
```

# Installation de Rancher pour gestion a distance de kubernetes

## Installer Rancher via docker-Compose

```sh
  user@master:~$: docker run --name rancher -d --restart=unless-stopped \
-p 81:80 -p 444:443 \
-v /opt/rancher:/var/lib/rancher \
rancher/rancher:latest #### to lauch rancher
user@master:~$: docker stop rancher && docker rm rancher #### to stop Rancher


```

### Acceder à  Rancher


* Accèder a rancher via: [https://ipserver:444](https://163.172.38.149:444)

1. rentrez un mot de passe
2. confirmer l'adresse https
3. add a cluster
4. click on import
5. enter a cluster name
6. click on create
7. copy de last line: curl......
8. enter this command in the cluster servers
9. verify in dashboard of kubernetes if cattle-cluster is Running
10. in dashboard of rancher click done and verify if cluster is running


# Installation de prometheus et grafana

1. click on cluster - default
2. click on Apps -> lauch
3. select your catalog
   - add personnal catalog on your github if you want to customize your chart
4. select Prometheus
5. Edit yaml configuration and past your data
  - an example of configuration is in ~/prepa-environnement/rancher/prometheus-config.yaml

6. click on Lauch
7. pour ajouter dans la config un nouveau metric d'application:
    - cliquer sur update prometheus et edit yaml et rajouter dans la section scrape_configs: le job comme indiquer dans le fichier yaml d'exemple

> you can install like this all applications that you can use to maintain your Stack
example you can install phpmyadmin for mysql or others RabitMQ ..... Mysqldump..

# Installer une connexion provisoire pour vous connecter a Redis ou MYSQL

### Installer un conteneur ubuntu

```sh
        user@master:~$: kubectl run -i --tty ubuntu-connect --image=ubuntu:16.04 --restart=Never -- bash -il
        root@ubuntu-connect:~$: apt-get update && apt-get install mysql-client -y  # Mysql Client
        root@ubuntu-connect:~$: apt-get update && apt-get install redis-server -y  # Redis Client
        root@ubuntu-connect:~$: mysql -h <mysql service>.namespace -u user -p to connect to mysql database
        mysql:3306> show databases;
        mysql:3306> exit
        root@ubuntu-connect:~$: redis-cli -h <redis service>.namespace
        redis-master.production:6379> set kuber awesome
        OK
        redis-master.production:6379> get kuber
        "awesome"
        redis-master.production:6379> exit
        root@ubuntu-connect:~$: exit
        user@master:~$: kubectl delete pods ubuntu-connect

```



# Installer le firewall UFW

- vérifier sudo user et les logs access
    ```sh
    user@server:~$: exit
    user@server:~$: sudo grep "Invalid" /var/log/auth.log

    ```

- voir plus haut notifier les ports accessibles



Et voilà !!! votre cluster kubernetes est prêt pour être connecté a GitLab.
L'installation d'autres composants se fera via l'interface GitLab lorsque la grappe de serveurs kubernetes sera connecté





# Préparation à la connexion GitLab - Kubernetes
Dans GitLab créer un nouveau projet avec pour template (nodejs express) reconnu compatible auto-devops
Une fois dans votre nouveau projet allez dans (menu a droite) Opérations -> kubernetes
(https://docs.gitlab.com/ee/user/project/clusters/#adding-an-existing-kubernetes-cluster)
* Ajouter une grappe de serveur kubernetes
* allez dans add existing cluster
* donnez un nom a votre grappe de serveur
* renseignez l'URL de l'API:
```sh
        user@master:~$: kubectl cluster-info | grep 'Kubernetes master' | awk '/http/ {print $NF}'
        Resultat:
					*************************************************************************
					https://xxx.xxx.xx.xx:6443
					*************************************************************************

```
* renseigner le certificat d'authorité de certification
```sh
        user@master:~$: kubectl get secrets
        Resultat:
					*************************************************************************
					NAME                  TYPE                                  DATA   AGE
                    default-token-862pg   kubernetes.io/service-account-token   3      29h
					*************************************************************************
		user@master:~$ kubectl get secret <secret name> -o jsonpath="{['data']['ca\.crt']}" | base64 --decode
		 Resultat:
					*************************************************************************
                    -----BEGIN CERTIFICATE-----
                    MI........yrbU=
                    -----END CERTIFICATE-----
                    *************************************************************************
```
* créer le service admin et renseigner son jeton
```sh
        user@master:~$: nano gitlab-admin-service-account.yaml
        -----------------------------------------------
         apiVersion: v1
         kind: ServiceAccount
         metadata:
           name: gitlab-admin
           namespace: kube-system
        -----------------------------------------------
        user@master:~$: kubectl apply -f gitlab-admin-service-account.yaml

        user@master:~$: nano gitlab-admin-cluster-role-binding.yaml
        ------------------------------------------------
         apiVersion: rbac.authorization.k8s.io/v1beta1
         kind: ClusterRoleBinding
         metadata:
           name: gitlab-admin
         roleRef:
           apiGroup: rbac.authorization.k8s.io
           kind: ClusterRole
           name: cluster-admin
         subjects:
         - kind: ServiceAccount
           name: gitlab-admin
           namespace: kube-system
         -------------------------------------------------
         user@master:~$: kubectl apply -f gitlab-admin-cluster-role-binding.yaml

         user@master:~$: kubectl -n kube-system describe secret $(kubectl -n kube-system get secret | grep gitlab-admin | awk '{print $1}')
        Resultat:
					*************************************************************************
					Name:         gitlab-admin-token-b5zv4
                    Namespace:    kube-system
                    Labels:       <none>
                    Annotations:  kubernetes.io/service-account.name=gitlab-admin
                                  kubernetes.io/service-account.uid=bcfe66ac-39be-11e8-97e8-026dce96b6e8

                    Type:  kubernetes.io/service-account-token

                    Data
                    ====
                    ca.crt:     1025 bytes
                    namespace:  11 bytes
                    token:      <authentication_token>

					*************************************************************************

```
* renseigner un espace de nom (celui de votre application)
* ajouter la grappe de serveur
* sur la page suivante renseigner la base domaine (un nom de domaine relier à l'ip de votre serveur master avec pour sous-domaine un astérix défini dans vos réglages DNS de votre nom de domaine)
* ensuite dans application n'installer que Helm Tiller
* une fois Helm Tiller d'installer, vous devez uniquement sur ce premier projet installer aussi le Runner-Gitlab via cette page. MAIS CECI UNIQUEMENT SUR CE PREMIER PROJET
* vous devez impérativement ne pas installer via cette page et pour tout les nouveaux projets que vous aller relier par la suite toutes les autres applications qui sont disponibles dans cette page config - NE PAS LES INSTALLER - SINON VOUS DEVREZ TOUT RECOMMENCER DEPUIS LE DEBUT....POUR TOUT LES AUTRES PROJETS QUE VOUS ALLEZ RELIER N'INSTALLER VIA CETTE PAGE CONFIG QUE HELM TILLER C'EST TOUT !!!
* POUR TOUT LES PROJETS SUIVANT INSTALLER LE RUNNER-GITLAB VIA LES REGLAGES CI/CD
    * dans le menu à droite de votre projet allez dans Paramètres -> intégration et livraison continue
    * étendre Executeur -> exécuteurs spécifiques -> configurer manuellement un executeur spécifique
      vous devriez trouver l'executeur installer sur le premier projet -> le selectionner pour l'activer

# Tester votre pipeline d'intégration continue
Dans votre projet allez dans le menu de droite Paramètres -> Intégration et livr... -> étendre AutoDevOps et l'activé
Ensuite vérifier votre pipeline est en route avec en prime le déployement en production de votre application sur votre cluster que vous pouvez conrôller via le Dashboard de Kubernetes !!

Voilà !!! That's all folks !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
